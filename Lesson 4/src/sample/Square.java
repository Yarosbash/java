package sample;

import java.util.Scanner;

public class Square {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Введите ширину прямоугольника");
		int w = sc.nextInt();
		System.out.println("Введите высоту прямоугольника");
		int h = sc.nextInt();
		
		for (int i = 1; i <= h; i++) {		
			
			for (int j = 1; j <= w; j++) {
				
				if ((j > 1 && j < w) && (i > 1 && i < h)) {
					System.out.print(" ");
				}else {
					System.out.print("*");
				}
			}			
			System.out.println();
		}

	}

}
