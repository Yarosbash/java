package sample;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Введите число от 4 до 16");
		int n = sc.nextInt();
		long fact = 1;
		
		if(n >= 4 && n <= 16) {
			for (int i = 1; i <= n; i++) {
				fact = fact * i;
		}
			
		System.out.println(n + "! = " + fact);
		}else {
			System.out.println("Число должно быть в пределах от 4 до 16");
		}
	}

}
