package sample;

import java.util.Arrays;
import java.util.Random;

public class Main {

  public static void main(String[] args) {
    Random rnd = new Random();

    int[] array = new int[10];

    int counter = 0;
    for (int i = 0; i < array.length; i++) {
      array[i] = rnd.nextInt(-5, 6);
      if (array[i] > 0) {
    	  counter++;
      }
    }
    System.out.println(Arrays.toString(array));
    System.out.println(counter);
    
    
  }

}
