package sample;

import java.util.Arrays;
import java.util.Random;

public class Matrix {

  public static void main(String[] args) {

    Random rn = new Random();

    int[][] matrix = new int[7][5];
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        matrix[i][j] = rn.nextInt(10);
      }
    }

    for (int i = 0; i < matrix.length; i++) {
      if (i == matrix.length / 2) {
        System.out.print("My matrix =    ");
      } else {
        System.out.print("               ");
      }
      System.out.println(Arrays.toString(matrix[i]));
    }

  }

}
