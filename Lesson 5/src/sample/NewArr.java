//2) Написать код для возможности создания массива целых чисел (размер вводиться с клавиатуры) и
//возможности заполнения каждого его элемента вручную. Выведите этот массив на экран.


package sample;

import java.util.Arrays;
import java.util.Scanner;

public class NewArr {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Введите замер массива");
		int value = sc.nextInt();		
		int arr [] = new int [value];
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println("Введите значение элемента " + (i+1));
			int item = sc.nextInt();
			arr[i] = item;
		}
		System.out.println("\n" + "Длина массива " + arr.length + " элементов");
		System.out.print("\n" + "Массив - ");
		System.out.print(Arrays.toString(arr));
	}

}
