//1) Написать код для зеркального переворота массива. Например (7,2,9,4) -> (4,9,2,7). Массив может
//быть произвольной длинны. Использовать дополнительный массив запрещено

package sample;

import java.util.Arrays;
import java.util.Scanner;

public class Reverse {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Введите длину массива");
		int length = sc.nextInt();
		int[] arr = new int [length];
		int[] newArr = new int [length]; 		
		int middle = (arr.length)/2; 
		int indexOne = 0;
		
		for(int i=0; i < arr.length; i++) {
			int item = (int)(Math.random()*100);
			arr[i] = item;
		}
		System.out.println("\n" + "Начальный массив");
		System.out.println(Arrays.toString(arr));
		
		for (int i=0; i < middle; i++) {
			indexOne = arr[i];
			arr[i] = arr[arr.length - 1 - i];
			arr[arr.length - 1 - i] = indexOne;
		}
		
		newArr = arr;
		System.out.println("\n" + "Конечный массив");
		System.out.println(Arrays.toString(newArr));
		
	}

}
