//3) Создать массив случайных чисел (размером 15 элементов). Создайте второй массив в два раза
//больше, первые 15 элементов должны быть равны элементам первого массива, а остальные
//элементы заполнить удвоенных значением начальных. Например:
//Было → {1,4,7,2}
//Стало → {1,4,7,2,2,8,14,4}

package sample;

import java.util.Arrays;
import java.util.Scanner;

public class DoubleArr {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Введите длину массива" + "\n");
		int length = sc.nextInt();
		int[] arr= new int[length];
		int [] newArr = Arrays.copyOf(arr, length * 2);		
		
		for (int i = 0; i < arr.length; i++) {
			int item =(int)(Math.random()*100);
			arr[i] = item;
			newArr[i] = item;
			newArr[length + i] = item * 2;		
		 }
		
		 System.out.print("\n" + "Начальный массив ");
		 System.out.println(Arrays.toString(arr));
		 System.out.print("\n" + "Новый массив ");
		 System.out.println(Arrays.toString(newArr));		 
		
	}
	
}
