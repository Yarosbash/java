// Пользователь вводит целое четырехзначное число. Проверить, является ли оно номером
//«счастливого билета». Примечание: счастливым билетом называется число, в котором, при четном
//количестве цифр в числе, сумма цифр его левой половины равна сумме цифр его правой
//половины. Например, рассмотрим число 1322. Его левая половина равна 13, а правая 22, и оно
//является счастливым билетом (т. к. 1 + 3 = 2 + 2).

package sample;

import java.util.Scanner;

public class Ticket {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Введите четырехзначное число");
		int ticketNum = sc.nextInt();	
		
		if (ticketNum < 1000 || ticketNum > 9999) {
			System.out.println("Введите ЧЕТЫРЕХЗНАЧНОЕ число!");
		}else {
			int digit1 = ticketNum / 1000;
			int digit2 = ticketNum / 100 % 10;
			int digit3 = ticketNum / 10 % 10;
			int digit4 = ticketNum % 10;
			
			if (digit1 + digit2 == digit3 + digit4) {
				System.out.println("У Вас счастливый билет!");
			}else {
				System.out.println("Ваш билет не счастливый...");
			}			
		}
	}
}
