package sample;

import java.util.Scanner;

public class Palindrom {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Введите шестизначное число");
		int num = sc.nextInt();	
		
		if (num < 100000 || num > 999999) {
			System.out.println("Введите ШЕСТИЗНАЧНОЕ число!");
		}else {
			int digit1 = num / 100000;
			int digit2 = num / 10000 % 10;
			int digit3 = num / 1000 % 10;
			int digit4 = num / 100 % 10;
			int digit5 = num / 10 % 10;
			int digit6 = num % 10;
			
			if (digit1 == digit6 && digit2 == digit5 && digit3 == digit4 ){
				System.out.println("Число - палиндром!");
			}else {
				System.out.println("Число -  не палиндром!");
			}			
		}
	}
}