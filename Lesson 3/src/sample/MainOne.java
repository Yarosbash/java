//2) Есть девятиэтажный дом, в котором 5 подъездов. Номер подъезда начинается с единицы. На
//одном этаже 4 квартиры. Напишите программу которая, получит номер квартиры с клавиатуры, и
//выведет на экран, на каком этаже, какого подъезда расположена эта квартира. Если такой
//квартиры нет в этом доме, то нужно сообщить об этом пользователю.


package sample;

import java.util.Scanner;

public class MainOne {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int allPod = 5;
		int allFloor = 9;
		int appsOnFloor = 4;
		int allApps = appsOnFloor * allFloor * allPod;
		System.out.println("Введите номер квартиры");
		
		int appart = sc.nextInt();
		
		int pod = appart / (allFloor * appsOnFloor) + 1;		
		int floor = (appart - (pod - 1) * (allFloor * appsOnFloor)) /  4 + 1;
		if (appart % 36 == 0) {
			pod -= 1;
			floor = appart / 4 / (pod);
			
		}
		System.out.println("Квартира находится в" + " " + pod + " " + "подъезде" + " " + "на" + " " + floor + " " + "этаже");
	
        if (appart <= 0 || appart > allApps) {
	        System.out.println("Такой квартиры в этом доме нету");
        }
	}
}

