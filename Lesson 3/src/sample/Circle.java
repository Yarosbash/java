package sample;

import java.util.Scanner;

public class Circle {

	public static void main(String[] args) {
	 Scanner sc = new Scanner(System.in);

	 double radius = 4;
	 System.out.println("Введите координату Х для точки");
	 double pointX = sc.nextDouble();
	  System.out.println("Введите координату Y для точки");
	 double pointY = sc.nextDouble();

	 if (Math.pow(pointX, 2) + Math.pow(pointY, 2) < Math.pow(radius, 2)) {
		 System.out.println("Точка лежит внутри круга");
	 }else {
		 System.out.println("Точка не лежит внутри круга");
	 }
	}
}
