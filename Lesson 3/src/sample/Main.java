//1) Написать программу которая считает 4 целых числа с клавиатуры и выведет на экран самое
//большое из них.

package sample;

import java.util.Scanner;

public class Main {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int digit1;
    int digit2;
    int digit3;
    int digit4;
    int maxDigit;

    System.out.println("Введите число 1");
    digit1 = sc.nextInt();
    System.out.println("Введите число 2");
    digit2 = sc.nextInt();
    System.out.println("Введите число 3");
    digit3 = sc.nextInt();
    System.out.println("Введите число 4");
    digit4 = sc.nextInt();

    maxDigit = digit1;
    if (digit2 > maxDigit) {
    	maxDigit = digit2;
    }
    if (digit3 > maxDigit) {
    	maxDigit = digit3;
    }
    
    if (digit4 > maxDigit) {
    	maxDigit = digit4;
    }

    System.out.println("Самое большое число = " + maxDigit);

  }
}
