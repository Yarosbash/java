package sample;

import java.util.Scanner;

public class Triangle {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
				int xPointA = 0;
				int yPointA = 0;				
				int xPointB = 4;
				int yPointB = 4;				
				int xPointC = 6;
				int yPointC = 1;				
				
				System.out.println("Введите координату Х:");
				double pointX = sc.nextDouble();
				System.out.println("Введите координату Y:");
				double pointY = sc.nextDouble();
				
				double scalA = (xPointA - pointX) * (yPointB - yPointA) - (xPointB - xPointA) * (yPointA - pointY);
				double scalB = (xPointB - pointX) * (yPointC - yPointB) - (xPointC - xPointB) * (yPointB - pointY);
				double scalC = (xPointC - pointX) * (yPointA - yPointC) - (xPointA - xPointC) * (yPointC - pointY);
				
				if ((scalA > 0 && scalB > 0 && scalC > 0) || (scalA < 0 && scalB < 0 && scalC < 0)) {
					System.out.println("Точка находится в пределах треугольника");
				} else if (scalA == 0 || scalB == 0 || scalC == 0){
					System.out.println("Точка лежит на одной из сторон и находится в пределах треугольника");
				} else {
					System.out.println("Точка находится за пределами треугольника");
				}
	}
}
