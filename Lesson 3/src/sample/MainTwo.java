//3) Треугольник существует только тогда, когда сумма любых двух его сторон больше третьей. Дано: a,
//b, c – стороны предполагаемого треугольника. Напишите программу, которая укажет, существует ли
//такой треугольник или нет.

package sample;

import java.util.Scanner;

public class MainTwo {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		    System.out.println("Введите длину стороны a");
            double a = sc.nextDouble();
            System.out.println("Введите длину стороны b");
            double b = sc.nextDouble();
            System.out.println("Введите длину стороны c");
            double c = sc.nextDouble();
            
            if (a > (b + c) || b > (a + c) || c > (a + b)){
            	System.out.println("Такой треугольник не существует!");
            } else {
            	System.out.println("Такой треугольник существует!");
            }

	}

}
