//Напишите метод который вернет максимальное число из массива целых чисел.

package sample;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class maxNum {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rnd = new Random();
        System.out.println("Введите длину массива" + "\n");
        int num = sc.nextInt();
        int [] arr = new int[num];        
        
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rnd.nextInt(-10, 50);
        }
        System.out.println("\n" + "Массив = " + Arrays.toString(arr));
        System.out.println("\n" + "Максимальное число = " + showMaxNumber(arr));
        
    }
        public static int showMaxNumber(int[] array) {
            int maxNum = 0;
            for (int i = 0; i < array.length; i++) {
                if (array[i] > maxNum) {
                    maxNum = array[i];
                }               
            }
            return maxNum;
        }
        
}
