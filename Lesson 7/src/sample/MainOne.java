package sample;

import java.util.Arrays;

public class MainOne {

  public static void main(String[] args) {

    long fact = calculateFactorial(6);
    System.out.println(fact);
    int[] array = new int[] { 0, 5, 1, 4, 8, 10, 3 };
    System.out.println(Arrays.toString(array));
    changeElement(array);
    System.out.println(Arrays.toString(array));

  }

  public static long calculateFactorial(int n) {
    long fact = 1;
    for (int i = 1; i <= n; i++) {
      fact = fact * i;
    }
    return fact;
  }

  public static void changeElement(int[] array) {
	    
		
		for (int i = 0; i < array.length; i++) {
			if (array[i] % 2 != 0) {
				array [i] = 0;
			}
		}
	
  }

}
