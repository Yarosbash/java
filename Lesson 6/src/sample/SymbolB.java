//1) Считайте строку текста с клавиатуры. Подсчитайте сколько раз в нем встречается символ «b».

package sample;

import java.util.Iterator;
import java.util.Scanner;

public class SymbolB {

	public static void main(String[] args) { Scanner sc = new Scanner(System.in);
		int counter =0;
		
		System.out.println("Input a strig");
		String text = sc.nextLine();
		char[] sym = text.toCharArray();
		
		for (int i = 0; i < sym.length; i++) {
			if(sym[i] == 'b') {
				counter++;
			}
			
		}
		System.out.println(counter);
	}

}
