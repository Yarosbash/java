//3) Выведите на экран 10 строк со значением числа Пи. Причем в первой строке должно быть 2 знака
//после запятой, во второй 3, в третьей 4 и т. д.

package sample;

import java.util.Iterator;


public class Pi {

	public static void main(String[] args) {
		
		int s = 0;
		
		for (int i = 0; i < 11; i++) {
			s = i;
			String text = String.format("%." + s + "f", Math.PI);
			System.out.println(text);
		}
	}

}
