package sample;

import java.util.Scanner;

public class Main {

  public static void main(String[] args) {

    Scanner sc = new Scanner(System.in);
    String nickName;
    System.out.println("Input nick name");
    nickName = sc.nextLine();

    String tempNickName = nickName.toUpperCase();
    char[] sym = tempNickName.toCharArray();

    boolean isCorrect = true;

    for (int i = 0; i < sym.length; i++) {
      if (!((sym[i] >= 'A' && sym[i] <= 'Z') || (sym[i] >= '0' && sym[i] <= '9') || (sym[i] == '_'))) {
        isCorrect = false;
        break;
      }
    }
    if (isCorrect) {
      System.out.println("nickName: " + nickName + " correct");
    } else {
      System.out.println("nickName: " + nickName + " incorrect");
    }
    
    int s = 8;

    String text = String.format("%." + s + "f", Math.PI);

    System.out.println(text);

  }

}
