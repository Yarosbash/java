//2) Вводится строка из слов, разделенных пробелами. Найти самое длинное слово и вывести его на
//экран

package sample;

import java.util.Arrays;
import java.util.Scanner;

public class LongWord {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Input a new string");
		String text = sc.nextLine();
		String[] result = text.split("[ ]");		
		String word = "";
		String bigWord = "";
		char[] symbols = {};
		int counter = 0;
		
		for (int i = 0; i < result.length; i++) {			
			word = result[i];			
			symbols = word.toCharArray();			
			
			for (int j = 0; j < symbols.length; j++) {
				counter++;
				if(symbols.length == counter) {
					bigWord = result[i];
				}
			}						
			
		}
		System.out.println("\n" + "Longest word is " + bigWord);		
	}

}
