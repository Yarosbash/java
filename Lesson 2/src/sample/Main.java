
package sample;

public class Main {

	public static void main(String[] args) {
		
		double sideA = 0.3;
		double sideB = 0.4;
		double sideC = 0.5;
		double p = 0.5*(sideA + sideB + sideC);		
		double S = Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC));
		double smallS = (Math.ceil(S*100)/100);
		
		System.out.println("Площадь треугольника равна" + " " + S);
		System.out.println("Округленная до двух знаков к большему значению площадь треугольника равна" + " " + smallS);
		
	}

}
