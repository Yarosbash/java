package sample;

import java.util.Scanner;

public class MainTwo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Введите стоимость 1 л. топлива, $");
		double fuelCost = sc.nextDouble();
		
		double fuelLHundKm = 8;
		double fuelLOneKm = fuelLHundKm/100;
		System.out.println("Ведите желаемое расстояние, км");
		double wayLong = sc.nextDouble();
		double fullWayFuel = (Math.ceil((wayLong * fuelLOneKm)*100)/100);
		double fullWayFuelCost = (Math.ceil((fullWayFuel * fuelCost)*100)/100);
		System.out.println("Вы израсходуете" + "  " + fullWayFuel + "л. топлива");
		System.out.println("Стоимость Вашей поездки составит" + "  " + fullWayFuelCost + "$"); 

	}

}
