package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class MainTwo {

  public static void main(String[] args) {
   
    File file = new File("array.txt");
    int[] array = new int[] { 5, 2, 15, 7, -3 };
    saveArrayToFile(file, array);

    String text = loadStringFromFile(file);
    System.out.println(text);
  }

  public static void saveArrayToFile(File file, int[] array) {
    try (PrintWriter pw = new PrintWriter(file)) {
      for (int i = 0; i < array.length; i++) {
        pw.println(array[i]);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static String loadStringFromFile(File file) {
    String result = "";
    try (Scanner sc = new Scanner(file)) {
      for (; sc.hasNextLine();) {
        result += sc.nextLine() + System.lineSeparator();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return result;
  }

}
