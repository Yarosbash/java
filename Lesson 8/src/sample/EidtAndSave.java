//1) Создайте консольный «текстовый редактор» с возможностью сохранения
//набранного текста в файл.

package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class EidtAndSave {

	public static void main(String[] args) {
		System.out.println("Input text");
		Scanner sc = new Scanner(System.in);
		String text = sc.nextLine();
		File fileOne = new File("Edit & Save.txt");
		editAndSaveText(fileOne, text);
	}
	public static void editAndSaveText(File file, String text) {
		try (PrintWriter pw = new PrintWriter(file)) {
			pw.println(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
