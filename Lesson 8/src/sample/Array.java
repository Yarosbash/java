//2) Напишите метод для сохранения в текстовый файл двухмерного массива целых
//чисел.

package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;

public class Array {

	public static void main(String[] args) {
		 
		File file1 = new File("ArrayTwo.txt");
		int[][] arr = new int[5][6];
		saveArrToFile(file1, arr);
		
	}
	public static void saveArrToFile(File file, int[][] array) {	
		
		Random rn = new Random();
		try (PrintWriter pw = new PrintWriter(file)){
			for (int i = 0; i < array.length; i++) {
				for (int j = 0; j < array[i].length; j++) {
					array[i][j] = rn.nextInt(10);
					}
					pw.println(Arrays.toString(array[i]));			
			}			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
